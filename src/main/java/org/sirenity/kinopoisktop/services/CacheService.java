package org.sirenity.kinopoisktop.services;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.sirenity.kinopoisktop.db.Film;
import org.sirenity.kinopoisktop.db.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class CacheService {
    private LoadingCache<Date, List<Film>> cache;

    @Autowired
    private FilmRepository filmRepository;

    public CacheService() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .refreshAfterWrite(1, TimeUnit.MINUTES)
                .build(new CacheLoader<Date, List<Film>>() {
                    public List<Film> load(Date key) {
                        return getFilmsFromDatabase(key);
                    }
                });
    }

    List<Film> getFilms(Date date) throws ExecutionException {
        return cache.get(date);
    }

    private List<Film> getFilmsFromDatabase(Date date) {
        return filmRepository.getTop10FilmsByDateOrderByPosition(date);
    }
}
