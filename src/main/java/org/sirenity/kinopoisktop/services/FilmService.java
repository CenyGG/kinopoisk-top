package org.sirenity.kinopoisktop.services;

import org.sirenity.kinopoisktop.db.Film;
import org.sirenity.kinopoisktop.db.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class FilmService {
    @Autowired
    private CacheService cacheService;

    public List<Film> getTop10(Date date) throws ExecutionException {
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);

        return cacheService.getFilms(new Date(now.getTimeInMillis()));
    }
}
