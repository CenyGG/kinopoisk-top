package org.sirenity.kinopoisktop.db;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface FilmRepository extends CrudRepository<Film, Integer> {
    List<Film> getTop10FilmsByDateOrderByPosition(Date date);
}
