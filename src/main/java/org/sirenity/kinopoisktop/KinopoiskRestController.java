package org.sirenity.kinopoisktop;

import org.sirenity.kinopoisktop.db.Film;
import org.sirenity.kinopoisktop.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
public class KinopoiskRestController {

    @Autowired
    private FilmService filmService;

    @RequestMapping("/api/top10")
    public List<Film> getTop10(String date) throws ExecutionException {
        if (date == null)
            return filmService.getTop10(new Date());
        else
            return filmService.getTop10(new Date(Long.parseLong(date)));

    }
}
