package org.sirenity.kinopoisktop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class KinopoiskTopApplication {

	public static void main(String[] args) {
		SpringApplication.run(KinopoiskTopApplication.class, args);
	}

}
