function updateRows(value) {
    $.get("/api/top10", {
            date: value
        })
        .done(function(result) {
            $("#top10table tbody").remove();
            $.each(result, function(i, item) {
                var $tr = $('<tr>').append(
                    $('<td>').text(item.position),
                    $('<td>').text(item.name),
                    $('<td>').text(item.year),
                    $('<td>').text(item.rating),
                    $('<td>').text(item.votes)
                ).appendTo('#top10table');
            });
        })
};

var currentDate = new Date()
$(function() {
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(date) {
            updateRows(new Date(date).getTime());
        }
    }).datepicker("setDate", currentDate);
});
$(updateRows(currentDate.getTime()))